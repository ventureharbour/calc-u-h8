import React from "react";
import "./App.css";
import Calculator from "./components/Calculator";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>CALC-U-H8</p>
      </header>
      <Calculator />
    </div>
  );
}

export default App;
